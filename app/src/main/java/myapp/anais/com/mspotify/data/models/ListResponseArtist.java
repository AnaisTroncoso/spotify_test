package myapp.anais.com.mspotify.data.models;

/**
 * Created by anaistroncoso on 18-05-17.
 */

public class ListResponseArtist {

    ListResponse artists;

    public ListResponse getArtists() {
        return artists;
    }

    public void setArtists(ListResponse artists) {
        this.artists = artists;
    }



}
