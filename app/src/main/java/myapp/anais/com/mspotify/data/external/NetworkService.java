package myapp.anais.com.mspotify.data.external;

import myapp.anais.com.mspotify.data.models.ListResponse;
import myapp.anais.com.mspotify.data.models.ListResponseArtist;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by anaistroncoso on 18-05-17.
 */

public interface NetworkService {

    @GET("/v1/search?type=artist")
    Observable<ListResponseArtist> getArtistList(@Query("q") String name, @Query("limit") int limit, @Query("offset") int offset);


    @GET("/v1/artists/{artistID}/albums")
    Observable<ListResponse> getAlbumList(@Path("artistID") String name, @Query("limit") int limit, @Query("offset") int offset);
}