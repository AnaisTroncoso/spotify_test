package myapp.anais.com.mspotify.mainflow.albums;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import myapp.anais.com.mspotify.R;
import myapp.anais.com.mspotify.data.models.Item;
import myapp.anais.com.mspotify.utils.OnClickAlbum;

/**
 * Created by anaistroncoso on 17-05-17.
 */

public class AlbumsRecyclerAdapter extends RecyclerView.Adapter {

    private final Context context;
    private final OnClickAlbum onClickAlbum;
    private final boolean showHeart;
    private List<Item> contents;

    private String TAG = getClass().getSimpleName();

    public AlbumsRecyclerAdapter(Context context, OnClickAlbum onClickAlbum, boolean showHeart) {
        this.context = context;
        this.onClickAlbum = onClickAlbum;
        this.showHeart= showHeart;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.albums_recycler_item, parent, false);
        return new MainRecyclerViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MainRecyclerViewHolder) holder).bind(contents.get(position));
    }

    @Override
    public int getItemCount() {
        return (contents == null || contents.isEmpty()) ? 0 : contents.size();
    }


    public void setContent(List<Item> contents) {
        this.contents = contents;
        notifyDataSetChanged();
    }


    public class MainRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imageView;
        TextView tvTitle, tvDescription;
        ImageButton imgHeart;
        Item content;

        public MainRecyclerViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvDescription = (TextView) itemView.findViewById(R.id.tv_description);
            imgHeart = (ImageButton) itemView.findViewById(R.id.img_heart);

        }

        public void bind(final Item content) {


            this.content = content;

            this.itemView.setOnClickListener(this);
            if (showHeart) {
                imgHeart.setOnClickListener(this);
            } else {
                imgHeart.setVisibility(View.GONE);
            }

            tvTitle.setText(content.getName());
            tvDescription.setText(content.getType());

            String images = content.getBiggestImage();

            Glide.with(context)
                    .load(images)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.ic_spotify_light)
                    .crossFade()
                    .skipMemoryCache(true)
                    .into(imageView);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.img_heart:
                    imgHeart.setBackground(ContextCompat.getDrawable(context, R.drawable.ic_heart_red));
                    onClickAlbum.onClick(content);
                    break;
                default:
                    onClickAlbum.onClick(content.getUri());
                    break;
            }
        }
    }


}
