package myapp.anais.com.mspotify.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by anaistroncoso on 18-05-17.
 */

public class Image extends SugarRecord<Image> implements Parcelable {
    int height;
    String url;
    int width;
    String id_item;


    public String getUrl() {
        return url;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.height);
        dest.writeString(this.url);
        dest.writeInt(this.width);
    }

    public Image() {
    }

    protected Image(Parcel in) {
        this.height = in.readInt();
        this.url = in.readString();
        this.width = in.readInt();
    }

    public static final Parcelable.Creator<Image> CREATOR = new Parcelable.Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel source) {
            return new Image(source);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };


    public void setItem(String item) {
        this.id_item = item;
    }

    public String getItem() {
        return id_item;
    }
}
