package myapp.anais.com.mspotify;

/**
 * Created by anaistroncoso on 17-05-17.
 */

public interface BasePresenter {

    void start();
}
