package myapp.anais.com.mspotify;

import javax.inject.Singleton;

import dagger.Component;
import myapp.anais.com.mspotify.data.external.NetworkModule;
import myapp.anais.com.mspotify.mainflow.MainActivity;

/**
 * Created by anaistroncoso on 18-05-17.
 */

@Singleton
@Component(modules = {NetworkModule.class,})
public interface Deps {
    void inject(MainActivity homeActivity);
}
