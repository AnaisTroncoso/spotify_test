package myapp.anais.com.mspotify.mainflow.albums;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import myapp.anais.com.mspotify.Constant;
import myapp.anais.com.mspotify.data.external.Service;
import myapp.anais.com.mspotify.data.models.Item;
import myapp.anais.com.mspotify.data.models.ListResponse;
import myapp.anais.com.mspotify.mainflow.artists.ArtistsFragment;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by anaistroncoso on 17-05-17.
 * <p>
 * Listens to user actions from the UI ({@link ArtistsFragment}), retrieves the data and updates the
 * UI as required.
 */

public class AlbumsPresenter implements AlbumsContract.Presenter {

    private final AlbumsContract.View mMainView;
    private final Service service;
    private CompositeSubscription subscriptions;
    private String TAG = getClass().getSimpleName();
    private int offset;
    private int total;
    private Boolean isLoading;


    public AlbumsPresenter(@NonNull AlbumsContract.View mMainView, Service mService) {
        this.mMainView = checkNotNull(mMainView, "view cannot be null");
        this.mMainView.setPresenter(this);
        this.service = mService;
        this.subscriptions = new CompositeSubscription();
        offset = 0;
        total = 0;
        isLoading = false;
    }


    @Override
    public void start() {
    }


    @Override
    public void result(int requestCode, int resultCode) {

    }

    @Override
    public void loadContent(String id) {

        if (isLoading)
            return;

        if (offset != 0 && offset >= total)
            return;

        if (total != 0)
            offset += Constant.LIMIT;

        isLoading = true;

        //Load the content
        Subscription subscription = service.getAlbumList(id, offset, new Service.ServiceGetCallback() {
            @Override
            public void onSuccess(ListResponse listResponse) {
                total = listResponse.getTotal();
                offset = listResponse.getOffset();
                List<Item> contentToShow = new ArrayList<>();
                contentToShow = listResponse.getItems();


                if (contentToShow != null && !contentToShow.isEmpty()) {
                    mMainView.showContent(contentToShow);
                } else {
                    if (offset == 0) {
                        mMainView.showNoContent(true);
                    }
                }
                isLoading = false;
            }

            @Override
            public void onError(Throwable networkError) {
                networkError.printStackTrace();
                mMainView.showLoadingIndicator(false);
                mMainView.showNoInternet(true);
                isLoading = false;
            }
        });

        subscriptions.add(subscription);
    }

    @Override
    public void saveAlbum(Item item) {
        Item.saveItem(item);
    }

    @Override
    public void deleteSaveAlbum(Item item) {
        item.delete();
    }

    @Override
    public void getSavedAlbum() {

        List<Item> items = Item.getAllItems();
        if (items != null && !items.isEmpty()) {
            mMainView.showContent(items);
        } else {
            mMainView.showNoSavedAlbums(true);
        }
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }

}
