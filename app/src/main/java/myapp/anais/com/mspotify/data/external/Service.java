package myapp.anais.com.mspotify.data.external;

import myapp.anais.com.mspotify.Constant;
import myapp.anais.com.mspotify.data.models.ListResponse;
import myapp.anais.com.mspotify.data.models.ListResponseArtist;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by anaistroncoso on 18-05-17.
 */

public class Service {
    private final NetworkService networkService;

    public Service(NetworkService networkService) {
        this.networkService = networkService;
    }

    public Subscription getArtistList(String name, int offset, final ServiceGetCallbackArtist callback) {

        return networkService.getArtistList(name, Constant.LIMIT, offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ListResponseArtist>>() {
                    @Override
                    public Observable<? extends ListResponseArtist> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ListResponseArtist>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(e);

                    }

                    @Override
                    public void onNext(ListResponseArtist listResponse) {
                        callback.onSuccess(listResponse);

                    }
                });
    }

    public Subscription getAlbumList(String name, int offset, final ServiceGetCallback callback) {

        return networkService.getAlbumList(name, Constant.LIMIT, offset )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ListResponse>>() {
                    @Override
                    public Observable<? extends ListResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ListResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(e);

                    }

                    @Override
                    public void onNext(ListResponse listResponse) {
                        callback.onSuccess(listResponse);

                    }
                });
    }


    public interface ServiceGetCallback {
        void onSuccess(ListResponse listResponse);

        void onError(Throwable networkError);
    }

    public interface ServiceGetCallbackArtist {
        void onSuccess(ListResponseArtist listResponse);

        void onError(Throwable networkError);
    }
}