package myapp.anais.com.mspotify.mainflow;

import myapp.anais.com.mspotify.data.models.Item;

/**
 * Created by anaistroncoso on 18-05-17.
 */

public interface OnClickArtist {

    void setOnClick(Item artist);
}
