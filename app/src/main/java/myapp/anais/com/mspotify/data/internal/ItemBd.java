package myapp.anais.com.mspotify.data.internal;

import com.orm.SugarRecord;

import java.util.List;

import myapp.anais.com.mspotify.data.models.Item;

/**
 * Created by anaistroncoso on 19-05-17.
 */

public class ItemBd extends SugarRecord<Item> implements SugarOperation {

    @Override
    public void saveEntity(ItemBd item) {
        item.save();
    }

    @Override
    public ItemBd getEntity(long i) {
        return ItemBd.findById(ItemBd.class, i);
    }

    @Override
    public List<ItemBd> getEntity(String i) {
        return ItemBd.find(ItemBd.class, "id_item = ? ", i);

    }

    @Override
    public void deleteEntity(ItemBd item) {
        item.delete();
    }

    @Override
    public List<ItemBd> listAllEntity() {
        return listAll(ItemBd.class);
    }

    @Override
    public void deleteEntity() {
        ItemBd.deleteAll(ItemBd.class);
    }

}
