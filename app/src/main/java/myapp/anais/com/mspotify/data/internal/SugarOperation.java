package myapp.anais.com.mspotify.data.internal;

import java.util.List;

/**
 * Created by anaistroncoso on 17-05-17.
 */

public interface SugarOperation {

    void saveEntity(ItemBd itemBd);

    Object getEntity(long i);

    void deleteEntity();

    Object listAllEntity();

    void deleteEntity(ItemBd itemBd);

    public List<ItemBd> getEntity(String i);

}
