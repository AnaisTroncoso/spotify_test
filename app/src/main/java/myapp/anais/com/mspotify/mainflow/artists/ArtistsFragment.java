package myapp.anais.com.mspotify.mainflow.artists;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.List;

import myapp.anais.com.mspotify.R;
import myapp.anais.com.mspotify.data.models.Item;
import myapp.anais.com.mspotify.mainflow.OnClickArtist;
import myapp.anais.com.mspotify.utils.ActivityUtils;
import myapp.anais.com.mspotify.utils.MessageUtils;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by anaistroncoso on 17-05-17.
 */

public class ArtistsFragment extends Fragment implements ArtistsContract.View, View.OnClickListener {


    private ArtistsContract.Presenter mPresenter;
    private RecyclerView mRecyclerView;
    private ArtistRecyclerAdapter mainRecyclerAdapter;
    private View progressBar;
    private View messageLayout;
    EditText mEditTextSearch;
    ImageButton mBtnSearch;
    OnClickArtist onClickArtist;

    public static ArtistsFragment newInstance() {
        ArtistsFragment mainFragment = new ArtistsFragment();
        return mainFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.artists_fragment, container, false);

        mRecyclerView = (RecyclerView) root.findViewById(R.id.recycler_content);
        progressBar = root.findViewById(R.id.progress_bar);
        messageLayout = root.findViewById(R.id.layout_message);
        mEditTextSearch = (EditText) root.findViewById(R.id.editText_search);
        mBtnSearch = (ImageButton) root.findViewById(R.id.btn_search);
        mBtnSearch.setOnClickListener(this);

        LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mainRecyclerAdapter = new ArtistRecyclerAdapter(getContext(), onClickArtist);
        mRecyclerView.setAdapter(mainRecyclerAdapter);

        mEditTextSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                String searchText = mEditTextSearch.getText().toString();
                if (!searchText.isEmpty()) {
                    mPresenter.search(searchText);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }


    @Override
    public void setPresenter(ArtistsContract.Presenter presenter) {
        this.mPresenter = checkNotNull(presenter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mPresenter.result(requestCode, resultCode);
    }

    @Override
    public void showLoadingIndicator(boolean active) {
        mRecyclerView.setVisibility(View.GONE);
        messageLayout.setVisibility(View.GONE);

        if (active) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showContent(List<Item> contents) {
        showContentViews(true);
        mainRecyclerAdapter.setContent(contents);
    }


    public void showContentViews(Boolean show) {
        if (show) {
            mRecyclerView.setVisibility(View.VISIBLE);
            messageLayout.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            messageLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showNoContent(Boolean show) {
        showContentViews(!show);
        MessageUtils.messageLayout(MessageUtils.ERROR_NOCONTENT, messageLayout);
    }

    @Override
    public void showNoInternet(Boolean show) {
        showContentViews(!show);
        MessageUtils.messageLayout(MessageUtils.ERROR_INTERNET, messageLayout);
    }

    @Override
    public void showSearchMessage(Boolean show) {
        showContentViews(!show);
        MessageUtils.messageLayout(MessageUtils.SEARCH, messageLayout);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_search) {
            String searchText = mEditTextSearch.getText().toString();
            if (!searchText.isEmpty()) {
                mPresenter.search(searchText);
            }
        }
    }


    public void setOnClickArtist(OnClickArtist onClickArtist) {
        this.onClickArtist = onClickArtist;
    }

    @Override
    public void onPause() {
        super.onPause();
        ActivityUtils.hideKeyboard(getActivity());
    }

}
