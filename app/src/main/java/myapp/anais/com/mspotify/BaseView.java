package myapp.anais.com.mspotify;

/**
 * Created by anaistroncoso on 17-05-17.
 */

public interface BaseView<T> {

    void setPresenter(T presenter);

    void showNoInternet(Boolean show);

    void showNoContent(Boolean show);

    void showLoadingIndicator(boolean active);

}