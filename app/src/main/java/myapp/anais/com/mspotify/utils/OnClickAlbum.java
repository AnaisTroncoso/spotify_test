package myapp.anais.com.mspotify.utils;

import myapp.anais.com.mspotify.data.models.Item;

/**
 * Created by anaistroncoso on 1-09-16.
 */
public interface OnClickAlbum {

    void onClick(Item item);

    void onClick(String uri);


}
