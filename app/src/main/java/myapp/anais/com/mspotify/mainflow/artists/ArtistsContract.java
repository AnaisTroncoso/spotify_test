package myapp.anais.com.mspotify.mainflow.artists;

import java.util.List;

import myapp.anais.com.mspotify.BasePresenter;
import myapp.anais.com.mspotify.BaseView;
import myapp.anais.com.mspotify.data.models.Item;

/**
 * Created by anaistroncoso on 17-05-17.
 * <p>
 * This specifies the contract between the view and the presenter.
 */

public interface ArtistsContract {

    interface Presenter extends BasePresenter {

        void result(int requestCode, int resultCode);

        void search(String name);

        void loadContent(boolean forceUpdate, boolean showLoadingUI, String name, int limit, int offset);

    }


    interface View extends BaseView<Presenter> {

        void showContent(List<Item> contents);

        void showContentViews(Boolean show);

        void showSearchMessage(Boolean show);

    }

}
