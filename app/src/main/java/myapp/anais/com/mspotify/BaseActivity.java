package myapp.anais.com.mspotify;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.io.File;

import myapp.anais.com.mspotify.data.external.NetworkModule;

/**
 * Created by anaistroncoso on 18-05-17.
 */

public class BaseActivity extends AppCompatActivity {
    Deps deps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File cacheFile = new File(getCacheDir(), "responses");
        deps = DaggerDeps.builder().networkModule(new NetworkModule(cacheFile)).build();

    }

    public Deps getDeps() {
        return deps;
    }
}
