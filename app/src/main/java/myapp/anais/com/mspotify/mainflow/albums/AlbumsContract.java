package myapp.anais.com.mspotify.mainflow.albums;

import java.util.List;

import myapp.anais.com.mspotify.BasePresenter;
import myapp.anais.com.mspotify.BaseView;
import myapp.anais.com.mspotify.data.models.Item;

/**
 * Created by anaistroncoso on 17-05-17.
 * <p>
 * This specifies the contract between the view and the presenter.
 */

public interface AlbumsContract {

    interface Presenter extends BasePresenter {

        void result(int requestCode, int resultCode);

        void loadContent(String id);

        void saveAlbum(Item item);

        void deleteSaveAlbum(Item item);

        void getSavedAlbum();
    }


    interface View extends BaseView<Presenter> {

        void showContent(List<Item> contents);

        void showContentViews(Boolean show);

        void showNoSavedAlbums(Boolean show);

    }

}
