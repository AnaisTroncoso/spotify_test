package myapp.anais.com.mspotify.mainflow.albums;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import myapp.anais.com.mspotify.R;
import myapp.anais.com.mspotify.data.models.Item;
import myapp.anais.com.mspotify.utils.MessageUtils;
import myapp.anais.com.mspotify.utils.OnClickAlbum;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by anaistroncoso on 17-05-17.
 */

public class AlbumsFragment extends Fragment implements AlbumsContract.View, OnClickAlbum {

    String TAG = getClass().getSimpleName();
    private AlbumsContract.Presenter mPresenter;
    private RecyclerView mRecyclerView;
    private AlbumsRecyclerAdapter mainRecyclerAdapter;
    private View progressBar;
    private View messageLayout;
    private Item artist;
    private ImageView image;


    public static AlbumsFragment newInstance(Item artist) {
        AlbumsFragment mainFragment = new AlbumsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("Artist", artist);
        mainFragment.setArguments(bundle);
        return mainFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (savedInstanceState != null) {
            artist = savedInstanceState.getParcelable("Artist");
        } else {
            artist = getArguments().getParcelable("Artist");
        }


        View root = inflater.inflate(R.layout.album_fragment, container, false);

        mRecyclerView = (RecyclerView) root.findViewById(R.id.recycler_content_album);
        progressBar = root.findViewById(R.id.progress_bar);
        messageLayout = root.findViewById(R.id.layout_message);
        image = (ImageView) root.findViewById(R.id.image_prevfull_phone);
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) root.findViewById(R.id.collapsing);

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        mRecyclerView.setLayoutManager(layoutManager);
        mainRecyclerAdapter = new AlbumsRecyclerAdapter(getContext(), this, (artist == null) ? false : true);
        mRecyclerView.setAdapter(mainRecyclerAdapter);

        if (artist != null) {
            collapsingToolbarLayout.setTitle(artist.getName());
            String images = artist.getBiggestImage();

            Glide.with(getContext())
                    .load(images)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.ic_spotify_light)
                    .crossFade()
                    .skipMemoryCache(true)
                    .into(image);

            showLoadingIndicator(true);

            mPresenter.loadContent(artist.getIdItem());
        } else {
            collapsingToolbarLayout.setVisibility(View.GONE);
            mPresenter.getSavedAlbum();
        }

        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("Artist", artist);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }


    @Override
    public void setPresenter(AlbumsContract.Presenter presenter) {
        this.mPresenter = checkNotNull(presenter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mPresenter.result(requestCode, resultCode);
    }

    @Override
    public void showLoadingIndicator(boolean active) {
        mRecyclerView.setVisibility(View.GONE);
        messageLayout.setVisibility(View.GONE);

        if (active) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showContent(List<Item> contents) {
        showLoadingIndicator(false);
        showContentViews(true);
        mainRecyclerAdapter.setContent(contents);
    }


    public void showContentViews(Boolean show) {
        if (show) {
            mRecyclerView.setVisibility(View.VISIBLE);
            messageLayout.setVisibility(View.GONE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
            messageLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showNoSavedAlbums(Boolean show) {
        showContentViews(!show);
        MessageUtils.messageLayout(MessageUtils.NO_SAVED_VIDEO, messageLayout);
    }

    @Override
    public void showNoContent(Boolean show) {
        showContentViews(!show);
        MessageUtils.messageLayout(MessageUtils.ERROR_NOCONTENT, messageLayout);
    }

    @Override
    public void showNoInternet(Boolean show) {
        showContentViews(!show);
        MessageUtils.messageLayout(MessageUtils.ERROR_INTERNET, messageLayout);
    }


    @Override
    public void onClick(Item item) {
        mPresenter.saveAlbum(item);
    }

    @Override
    public void onClick(String uri) {
        Intent launcher = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(launcher);
    }
}
