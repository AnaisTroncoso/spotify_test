package myapp.anais.com.mspotify.mainflow.artists;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import myapp.anais.com.mspotify.R;
import myapp.anais.com.mspotify.data.models.Item;
import myapp.anais.com.mspotify.mainflow.OnClickArtist;

/**
 * Created by anaistroncoso on 17-05-17.
 */

public class ArtistRecyclerAdapter extends RecyclerView.Adapter {

    private final Context context;
    private final OnClickArtist onClickArtist;
    private List<Item> contents;


    public ArtistRecyclerAdapter(Context context, OnClickArtist onClickArtist) {
        this.context = context;
        this.onClickArtist = onClickArtist;
    }

    public void setContent(List<Item> contents) {
        this.contents = contents;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.artists_recycler_item, parent, false);
        return new MainRecyclerViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MainRecyclerViewHolder) holder).bind(contents.get(position));
    }

    @Override
    public int getItemCount() {
        return (contents == null || contents.isEmpty()) ? 0 : contents.size();
    }

    public class MainRecyclerViewHolder extends RecyclerView.ViewHolder {

        View itemView;
        ImageView imageView;
        TextView tvTitle, tvDescription;

        public MainRecyclerViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvDescription = (TextView) itemView.findViewById(R.id.tv_description);
            this.itemView = itemView;
        }

        public void bind(final Item content) {
            tvTitle.setText(content.getName());
            tvDescription.setText(content.getType());

            String images = content.getSmallerImage();

            Glide.with(context)
                    .load(images)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.ic_spotify_light)
                    .bitmapTransform(new CropCircleTransformation(context))
                    .crossFade()
                    .skipMemoryCache(true)
                    .into(imageView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickArtist.setOnClick(content);
                }
            });
        }
    }
}
