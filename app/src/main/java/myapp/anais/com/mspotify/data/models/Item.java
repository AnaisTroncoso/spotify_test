package myapp.anais.com.mspotify.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by anaistroncoso on 18-05-17.
 */


public class Item extends SugarRecord<Item> implements Parcelable {

    private transient Long id;
    @SerializedName("id")
    String id_item;
    List<Image> images;
    String name;
    int popularity;
    String type;
    String uri;


    public String getIdItem() {
        return id_item;
    }

    public void setIdItem(String id) {
        this.id_item = id;
    }

    public List<Image> getImages() {
        if (images == null)
            images = Image.find(Image.class, "IDITEM = ?", id_item);

        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }


    public String getSmallerImage() {
        Collections.sort(this.getImages(), new Comparator<Image>() {
            @Override
            public int compare(Image firts, Image second) {
                return Integer.valueOf(firts.height).compareTo(second.height);
            }
        });

        return (this.getImages() != null && !this.images.isEmpty()) ? this.getImages().get(0).getUrl() : "";
    }

    public String getBiggestImage() {
        if (this.getImages() == null)
            return "";

        Collections.sort(this.getImages(), new Comparator<Image>() {
            @Override
            public int compare(Image firts, Image second) {
                return Integer.valueOf(second.height).compareTo(firts.height);
            }
        });
        return (this.getImages() != null && !this.images.isEmpty()) ? this.getImages().get(0).getUrl() : "";
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id_item);
        dest.writeList(this.images);
        dest.writeString(this.name);
        dest.writeInt(this.popularity);
        dest.writeString(this.type);
        dest.writeString(this.uri);
    }

    public Item() {
    }

    protected Item(Parcel in) {
        this.id_item = in.readString();
        this.images = new ArrayList<Image>();
        in.readList(this.images, Image.class.getClassLoader());
        this.name = in.readString();
        this.popularity = in.readInt();
        this.type = in.readString();
        this.uri = in.readString();
    }

    public static final Parcelable.Creator<Item> CREATOR = new Parcelable.Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel source) {
            return new Item(source);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    public static List<Item> getAllItems() {
        return Item.listAll(Item.class);
    }

    public static List<Item> getAllItems(String id) {
        return Item.find(Item.class, "id_item = ?", id);
    }

    public static void saveItem(final Item item) {
        new Thread(new Runnable() {
            public void run() {
                for (Image image : item.getImages()) {
                    image.setItem(item.getIdItem());
                    image.save();
                }
                item.save();
            }
        }).start();
    }

}
