package myapp.anais.com.mspotify.mainflow;

import android.support.design.widget.TabLayout;
import android.os.Bundle;

import javax.inject.Inject;

import myapp.anais.com.mspotify.BaseActivity;
import myapp.anais.com.mspotify.ChangeFlow;
import myapp.anais.com.mspotify.R;
import myapp.anais.com.mspotify.data.external.Service;
import myapp.anais.com.mspotify.data.models.Item;
import myapp.anais.com.mspotify.mainflow.albums.AlbumsFragment;
import myapp.anais.com.mspotify.mainflow.albums.AlbumsPresenter;
import myapp.anais.com.mspotify.mainflow.artists.ArtistsFragment;
import myapp.anais.com.mspotify.mainflow.artists.ArtistsPresenter;
import myapp.anais.com.mspotify.utils.ActivityUtils;


public class MainActivity extends BaseActivity implements ChangeFlow {
    @Inject
    public Service service;

    private ArtistsPresenter mArtistPresenter;
    private AlbumsPresenter mAlbumsPresenter;
    private TabLayout mTabLayout;

    private TabLayout.OnTabSelectedListener mTabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            setFlow(mTabLayout.getSelectedTabPosition());
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        mTabLayout = (TabLayout) findViewById(R.id.tabLayout);
        mTabLayout.addOnTabSelectedListener(mTabSelectedListener);

        getDeps().inject(this);

        setFlow(0);
    }


    private String TAG = getClass().getSimpleName();
    OnClickArtist onClickArtist = new OnClickArtist() {
        @Override
        public void setOnClick(Item artist) {
            AlbumsFragment albumFragment;
            albumFragment = AlbumsFragment.newInstance(artist);
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), albumFragment, R.id.content_frame);

            // Create the presenter
            mAlbumsPresenter = new AlbumsPresenter(albumFragment, service);
            ActivityUtils.hideKeyboard(MainActivity.this);
        }
    };

    @Override
    public void setFlow(int flow) {
        switch (flow) {
            case 0: //Main Flow
                ArtistsFragment mainFragment;
                mainFragment = ArtistsFragment.newInstance();
                mainFragment.setOnClickArtist(onClickArtist);
                ActivityUtils.replaceFragmentToActivity(
                        getSupportFragmentManager(), mainFragment, R.id.content_frame);

                // Create the presenter
                mArtistPresenter = new ArtistsPresenter(mainFragment, service);
                break;
            case 1:
                AlbumsFragment albumFragment;
                albumFragment = AlbumsFragment.newInstance(null);
                ActivityUtils.replaceFragmentToActivity(
                        getSupportFragmentManager(), albumFragment, R.id.content_frame);

                // Create the presenter
                mAlbumsPresenter = new AlbumsPresenter(albumFragment, service);

                break;

        }
    }
}
