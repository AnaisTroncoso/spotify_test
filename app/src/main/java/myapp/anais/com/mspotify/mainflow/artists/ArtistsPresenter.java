package myapp.anais.com.mspotify.mainflow.artists;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import myapp.anais.com.mspotify.Constant;
import myapp.anais.com.mspotify.data.external.Service;
import myapp.anais.com.mspotify.data.models.Item;
import myapp.anais.com.mspotify.data.models.ListResponseArtist;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by anaistroncoso on 17-05-17.
 * <p>
 * Listens to user actions from the UI ({@link ArtistsFragment}), retrieves the data and updates the
 * UI as required.
 */

public class ArtistsPresenter implements ArtistsContract.Presenter {

    private final ArtistsContract.View mMainView;
    private final Service service;
    private CompositeSubscription subscriptions;

    public ArtistsPresenter(@NonNull ArtistsContract.View mMainView, Service mService) {
        this.mMainView = checkNotNull(mMainView, "view cannot be null");
        this.mMainView.setPresenter(this);
        this.service = mService;
        this.subscriptions = new CompositeSubscription();
    }


    @Override
    public void start() {
        mMainView.showSearchMessage(true);
    }


    @Override
    public void result(int requestCode, int resultCode) {

    }

    @Override
    public void search(String name) {
        loadContent(true, true, name, Constant.LIMIT, Constant.OFFSET);
    }


    @Override
    public void loadContent(boolean forceUpdate, final boolean showLoadingUI, String name, int limit, int offset) {
        if (showLoadingUI) {
            mMainView.showLoadingIndicator(true);
        }

        //Load the content
        Subscription subscription = service.getArtistList(name, offset, new Service.ServiceGetCallbackArtist() {
            @Override
            public void onSuccess(ListResponseArtist listResponseArtist) {
                List<Item> contentToShow = new ArrayList<>();
                contentToShow = listResponseArtist.getArtists().getItems();
                if (showLoadingUI) {
                    mMainView.showLoadingIndicator(false);
                }
                if (contentToShow != null && !contentToShow.isEmpty()) {
                    mMainView.showContent(contentToShow);
                } else {
                    mMainView.showNoContent(true);
                }
            }

            @Override
            public void onError(Throwable networkError) {

                if (showLoadingUI) {
                    mMainView.showLoadingIndicator(false);
                }
                mMainView.showNoInternet(true);
            }
        });

        subscriptions.add(subscription);
    }

    public void onStop() {
        subscriptions.unsubscribe();
    }

}
