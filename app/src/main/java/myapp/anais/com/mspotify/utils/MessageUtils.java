package myapp.anais.com.mspotify.utils;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import myapp.anais.com.mspotify.R;


/**
 * Created by anaistroncoso on 18-05-17.
 */

public class MessageUtils {

    public static int SEARCH = 1;
    public static int ERROR_INTERNET = 2;
    public static int ERROR_NOCONTENT = 3;
    public static int NO_SAVED_VIDEO = 4;


    public static void messageLayout(int type, View view) {

        ImageView image = (ImageView) view.findViewById(R.id.imageView_message);
        TextView textView = (TextView) view.findViewById(R.id.textView_message);

        switch (type) {
            case 1:
                image.setImageResource(R.drawable.ic_spotify);
                textView.setText(R.string.search_artist);
                break;
            case 2:
                image.setImageResource(R.drawable.ic_spotify);
                textView.setText(R.string.error_internet);
                break;
            case 3:
                image.setImageResource(R.drawable.ic_spotify);
                textView.setText(R.string.no_content);
                break;
            case 4:
                image.setImageResource(R.drawable.ic_spotify);
                textView.setText(R.string.no_saved_album);
                break;

        }
    }
}
